# Mazda 3 / 6 / RX-8 Navigation Display Notes

## Overview

Screen is NTSC (-J?), RGBS input. TFT with CCFL backlight.

320x240-ish (seems to be 320x224). Unsure of bit depth. 60Hz

Connects to the Remote (Illumination, NEC IR encoding - without the physical IR), Navigation computer (RGBS, UART) and to the Audio unit (also UART).

(Mazda 3) Built into the centre dashboard vents panel. Has OPEN and TILT buttons. Uses ~1A open and 300mA shut (B+ and ACC) and nil with ACC off.

## Connections

### Power

Nothing special.

12V negative ground. B+ and ACC inputs.


### Illumination

Hooks into the normal Mazda TNS / ILLUM wires.

This is wired to the remote.


### Video (Navigation Computer)

Takes ~1vP-P separate R, G, B and Combined Sync.

RGB with HV sync can be combined and it does work [with a simple adaptor](https://www.retrorgb.com/building-a-passive-sync-combiner.html)

15.85 KHz HSync

60 Hz VSync

Can be done with a Raspberry Pi 4, VGA666 and HVSync-to-CSync.


### Video (Reversing camera)

Not used in Mazda Vehicles. Has [been demonstrated to work](https://web.archive.org/web/20220706185955/https://www.mazda3forums.com/threads/adding-tv-out-on-mz3-with-oem-navigation-designed-for-rearview-camera.369785/page-2) before I found it.

The screen has three inputs of interest on the 24-pin connector:

 - M "REAR CAM"
 - T "REV"
 - H "COMPOSITE"

"REAR CAM" appears to be a simple pull up / down to indicate that a camera is present.

"REV" switches the video to the "COMPOSITE" input.

"COMPOSITE" should be the video input.

I have not yet tested this.


### Serial (Navigation Computer)

5V UART, 9600 baud. Two lines (one either way)

Protocol not understood yet, but does look to send key presses from the remote to the Navigation computer.

Screen sends a ~4 byte status / heartbeat message that does indicate if it's open or not.

I believe I've seen the screen open under command by the navigation system, so it might be possible to control the open / tilt status of the screen as well.


### Serial (Audio system)

Not captured / recorded / investigated yet.

Unsure of purpose. The Navigation computer does have a "beep" configuration option, so I suspect it commands the radio to send beeps to the speakers.

The Navigation system has reverse and speed inputs, so there'd be little else it'd need from the CAN BUS.


### IR Input

Recieves data from the wired "remote" control. Accepts NEC2 commands with an address of 0xEA81. Since it's wired, it does not like seeing a "carrier". 5V, Pull down.

| Command | Button name | Role |
| ------- | ----------- | ---- |
| 0x00 | (D-Pad up) | Move up |
| 0x01 | (D-Pad left) | Move left |
| 0x02 | (D-Pad right) | Move right |
| 0x03 | (D-Pad down) | Move down |
| 0x04 | (D-Pad up/left) | Move diagonally up and left |
| 0x05 | (D-Pad up/right) | Move diagonally up and right |
| 0x06 | (D-Pad down/right) | Move diagonally down and right |
| 0x07 | (D-Pad down/left) | Move diagonally down and left |
| 0x08 | ENTER | Select highlighted item or show pop-up menu |
| 0x09 | RET | Go back in the menu |
| 0x0A | "^" | Zoom out |
| 0x0B | "v" | Zoom in |
| 0x0C | MENU | Opens the main menu |
| 0x0D | POS | Go to the map screen |
| 0x0E | VOICE | Repeat the voice prompt |
| 0x30 | DIM | Dims the screen (might only work when TNS is on?) |

There is a conspicuous black plastic section on the right of the screen, so it's probable that this screen also accepts real IR.


