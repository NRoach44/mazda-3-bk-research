/*
 * SimpleSender.cpp
 *
 *  Demonstrates sending IR codes in standard format with address and command
 *  An extended example for sending can be found as SendDemo.
 *
 *  Copyright (C) 2020-2022  Armin Joachimsmeyer
 *  armin.joachimsmeyer@gmail.com
 *
 *  This file is part of Arduino-IRremote https://github.com/Arduino-IRremote/Arduino-IRremote.
 *
 *  MIT License
 */
// Significant changes by NRoach44
// Also under MIT

// Refer to https://learn.adafruit.com/using-an-infrared-library/sending-ir-codes
// Connect the Navigation Display's remote data input to the collector (top) of the NPN transistor.

#include <Arduino.h>

#define SEND_PWM_BY_TIMER         // Disable carrier PWM generation in software and use (restricted) hardware PWM. This is required for duty cycle control below
#define IR_SEND_DUTY_CYCLE_PERCENT 100  // Wired input expects simple on or off pulses and no carrier

#include "PinDefinitionsAndMore.h" // Define macros for input and output pin etc.
#include <IRremote.hpp>

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);

    Serial.begin(115200);

    // Just to know which program is running on my Arduino
    Serial.println(F("START " __FILE__ " from " __DATE__ "\r\nUsing library version " VERSION_IRREMOTE));
    Serial.print(F("Send IR signals at pin "));
    Serial.println(IR_SEND_PIN);
}

// To suit Mazda 3 Navigation Display BR9L 66 DV0 (Panasonic CN-TM6590A)
// Should also work with Mazda 5/MPV, 6, RX-8 of similar vintage (2004-2008/2014ish)
// Remote BK4K 66 9L1 (Panasonic CY-KM6390A) is an IR remote except the "output" to the diode
// is wired direct to the display. Display appears to have IR reciever built in  - yet to test.
uint16_t iAddress = 0xEA81;
uint8_t iCommand = 0x00;
uint8_t iRepeats = 0;

/* Recorded commands:
00  D-Pad up
01  D-Pad left
02  D-Pad right
03  D-Pad down
04  D-Pad up & left
05  D-Pad up & right
06  D-Pad down & right
07  D-Pad down & left
08  "ENTER" (press the D-Pad knob in)
09  "RET" (back)
0A  "^" (zoom OUT)
0B  "v" (zoom IN)
0C  "MENU"
0D  "POS" (back to map display)
0E  "VOICE" (repeat text-to-speech command)
30  "DIM" (toggles dark mode - doesn't work unless TNS is high(?))
*/

// Seems to work with 0 repeats, at least for menu navigation.

void loop() {
  String readString;
  String sData;
  String sRepeat;

  // Read in a string - we're expecting it to be aabb where 
  // aa == command / data as two digit base-10 number and bb == number of repeats
  // e.g. '0803'
  if (Serial.available() > 0) {
    uint8_t waitForNewline = 1;
    while (waitForNewline) {
      delay(3);
      if (Serial.available() >0) {
        char c = Serial.read();
        if (c == 10){
          waitForNewline = 0;
        } else {
          readString += c;
        }
      }
    }

    // prints the received data
    Serial.print("SER IN:");
    Serial.println(readString);
    
    // split the input into two strings and attempt parsing
    sData   = readString.substring(0,2);
    sRepeat = readString.substring(2,4);
    iCommand = sData.toInt();
    iRepeats = sRepeat.toInt();

    // report what we've parsed it as
    Serial.print(F("PARSED: "));
    Serial.print(iCommand);
    Serial.print(F(", "));
    Serial.print(iRepeats);
    Serial.println();
    Serial.flush();

    // send IR code
    Serial.print(F("IR OUT: A:0x"));
    Serial.print(iAddress, HEX);
    Serial.print(F(", D:0x"));
    Serial.print(iCommand, HEX);
    Serial.print(F(", R:"));
    Serial.print(iRepeats);
    Serial.println();
    Serial.flush();

    IrSender.sendNEC2(iAddress, iCommand, iRepeats);

    delay(1000);
  }
}
