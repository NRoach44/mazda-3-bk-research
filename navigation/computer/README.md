# Mazda 3 / 6 / RX-8 Navigation Computer Notes

## Overview

Proprietary DVD-ROM based GPS navigation system.

Contains IMU, GPS, RTC.

Reads speed from the instrument cluster, reverse gear from harness.

Contains Hitachi SuperH-3 CPU, RAM, flash, ATA DVD ROM.

Can't find any community work on "opening up" this system.

All "verified" information here has been tested on a CN-VM4360A from a 2004 Mazda 3 BK. Date stamped January 2004.

### Variants

Major variation is that facelift Mazda 3s (JMxBKxxx2, after mid 2006) have a DENSO unit, as opposed to a Panasonic unit. I believe they should be mostly compatible with what is described here.


### Hidden menus

#### Diagnostic Info

Menu > Setup > System Information > Version

UP > UP > DOWN > DOWN

Displays "Wiring Check" and "GPS" options

##### Wiring check

Shows

* Headlight status (on/off)
* GPS status (on/off)
* Parking brake (on/off)
* Back-up light (on/off)
* Speed (0-200KM/h, bar with scale)
* ACC Volt (displayed as a number (e.g. 12.4V))
* Speed pulse (displayed as a four digit number)
* Accuracy Level ("Level 4")

##### GPS

Shows GPS status (Lat / Long, Date & Time, map with satellites)

#### NAVI ECU Information Menu

Menu > Setup > System Information > Version

UP > UP > UP > DOWN > DOWN > DOWN > UP > UP

##### Internal Coefficient

Looks to be adjustment for the vehicle's speed pulse - set the distance the car travels per pulse, and allow automatic correction

##### Vehicle Signal (TMC)

Looks to be another way of displaying inputs to the unit.

###### Rotation Check

Calibrates the unit by rotating the car slowly?

###### TMC

Can see "Receiving Information" (adjust the FM RDS TMC receiver settings) and Country Restriction (allows toggling reception in those countries?).

##### Software Version

Get lifetime counter info:

* NAVI Operation Time (seconds)
* Matching Time (Seconds)
* GPS Measure Time (Seconds)
* Num of CD-ROM Access (counts)
* Num of CD-ROM Error (counts)
* Travel Distance (km)
* Location Error position (Start / End)
* Matching GPS (Start / end)

###### Ver. Info

| Item name | Note | Value |
| --------- | ---- | ----- |
| Main CPU (M) | Ver | U4.06 2002.9 |
| Main CPU (F) | Ver | U9.138 2003.7 |
| Sub CPU | Ver | 82 |
| CD-ROM Drive | Ver | 2.04 |
| GPS | Ver | 0.0 |
| Map Database | | 1.7.27.6 |
| Map Software | | 1.7.32.3 |

###### Map Area

Select between "Valid DCA Only" and "All DCAs".

##### Display Check

Shows Colour bars and allows showing all-white.

##### GPS Info

Even more information about GPS reception.

###### Vehicle Information

Shows current status (lat / long / alt / speed)

##### System Check

| Left | Right |
| ---- | ----- |
| NAVI Control | 0000000A |
| CD ROM | 04045300 |
| GPS | 030000004|
| Sensor | OK|

##### Diagnosis Memory

Shows error log history for the following:

* ECU
* NAVI
* CD
* GPS
* Sensor
* SubCPU
* MPEG
* TMC

##### CD Check

Tests the audio output - "Chime Sound" and "Vehicle Guidance" buttons click the audio output relay.

Cold Start appears to restart the device (and possibly wipe all user data?)

#### System Analysis Menu

##### VL Menu

Shows menus / buttons related to recording and playback

* Start Record
* Start Playback
* Start Logging
* Start Dump
* Shutdown
* Init VP
* Cold Start VP

##### RD Menu

* Fast Reroute (On / Off)
* Regular Reroute (On / Off)
* When Dist2Dest is From (number input) km To (number input) km

##### Serial I/O Menu

* (Blank button)
* Enable ALL Serial
* Enable ALL Except
* Enable DE
* Enable HC
* Enable MG
* Enable MN
* Enable MR
* Enable MU
* Enable RD
* Enable SE
* Enable SI
* Enable UI
* Enable VL
* Enable D List


## Connections

### Power

Nothing special.

12V negative ground. B+ and ACC inputs.


### Illumination

Not wired in the Mazda 3 at least.

Looks to connect to the normal Mazda TNS / ILLUM wires.

Illumination status appears to be read through the display (?)

### Video output

~1vP-P separate R, G, B and Combined Sync.

15.85 KHz HSync

60 Hz VSync

320x240p. (224p?)

Can be done with a Raspberry Pi 4, VGA666 and HVSync-to-CSync.


### Video (Reversing camera)

Not used in Mazda Vehicles. Has [been demonstrated to work](https://web.archive.org/web/20220706185955/https://www.mazda3forums.com/threads/adding-tv-out-on-mz3-with-oem-navigation-designed-for-rearview-camera.369785/page-2) before I found it.

The screen has three inputs of interest on the 24-pin connector:

 - M "REAR CAM"
 - T "REV"
 - H "COMPOSITE"

"REAR CAM" appears to be a simple pull up / down to indicate that a camera is present.

"REV" switches the video to the "COMPOSITE" input.

"COMPOSITE" should be the video input.

I have not yet tested this.


### Serial (Navigation Display)

5V UART, 9600 baud. Two lines (one either way)

Protocol not understood yet.

Screen sends a ~4 byte status / heartbeat message that does indicate if it's open or not.

I believe I've seen the screen open under command by the navigation system, so it might be possible to control the open / tilt status of the screen as well.

Remote control data and possibly data from / to the Audio Unit travels via these lines.

When the display is shut, the remote doesn't work. Unknown if the display is blocking the messages, or if the computer ignores them when it knows the display is closed.

### Serial (RDS(?))

Not captured / recorded / investigated yet.

8-pin connector has power and "TX-DATA"/RX-DATA. Suspect (based on some notes seen somewhere) that this may be an RDS receiver?


### Speed signal

Unknown type of signal. Suspect it's a typical "pulse per wheel rotation" style signal. Originates in Instrument Cluster.


### Reverse input

Hooked to the reversing lights. Indicates that the car is moving backwards.


### Parking Brake

What is says on the can. Input from the PJB / harness indicating that the parking break is on.


### Audio input / output

Intercepts the output of the Audio system to the driver's tweeter. "Speaks" instructions to the driver. Can hear a relay click when activated.


### DVD-ROM

Accepts "NAVTECH" SDAL map disks. Format is apparently open, but there doesn't appear to be any third party / community made disks.
