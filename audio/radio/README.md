# Mazda 3 BK Radio Notes

## Overview

Mazda used a generic "framework" of modules for these cars, which appears to be reasonably compatible between late-90s until late 2000s vehicles.

In the Mazda 3, the "Audio Unit" consists of up to five modules.

* Fascia
* LCD (a.k.a Driver Information Display / System, connected to fascia (-05?) or direct to MSCAN (05+?))
* Radio and Amplifier module (not optional, may offload amplifying to a separate BOSE amp)
* CD Module (Upper, optional, may be a six CD stacker unit, may also support MP3 CDs (but then it's intergrated into the radio module?))
* Tape Cassette / Mini-Disc Module (Lower, optional. In some very limited situations it may be a 20GB HDD, not known if a module or not)

The radios in the facelift models (2006-06 onwards) combine the CD and core modules.

The following model numbers are known to me:

| ASM PN | Tuner Vendor | Tuner Model Number | Tuner Part Number | CD? | Tape? | W/ Trip | W/ Nav | W/ BOSE | AUX-In | LCD Connection | Original Vehicle notes |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | -- |
| `BN8S 66 9RXA` | FMS | 14789959 | `BP0S 66 AS0` | `GJ6J 66 EG0` (Panasonic 6CD Non-MP3) | None | No | Yes | No | Ext CD CHG Only | via fascia | 2004 Mazda 3, USDM (LHD), w/ Navigation (Numbers on the right)|
| `BP0L 66 9RXA` | FMS | 14789959 | `BP0S 66 AS0` | `GJ6J 66 EG0` (Panasonic 6CD Non-MP3) | None | No | No  | No | Ext CD CHG Only | via fascia | 2004 Mazda 3, AUDM (RHD), w/o Navigation (Numbers on the left)|
| `BP4L 66 9S0A` | | | | 1CD | | | | | 2004 Mazda 3, UKDM (RHD) (Numbers on the right)
| | | | `BP4K 66 AS0`  | | | | | No  | | | 2004 Mazda 3, Taiwan/Philippines/Vietnam (LHD) (5/100KHz) |
| | | | `BVSF 66 AS0`  | | | | | No  | | | 2004 Mazda 3, Columbia (LHD) (5/100KHz) |
| `BP0K 66 9R0A` | FMS (?) | | | 1CD | No | No | | | No | Fascia | 2004 Mazda 3, AUDM |
| `BN8F 66 9R0A` | | | | 1CD | None | No | No | No | No | Fascia | 2004 Mazda 3, USDM (Before 01.08.2004) |
| `BN8K 66 9R0` | | | | 1CD | None | No | No | No | No | Fascia | 2004 Mazda 3, USDM (After 01.08.2004) |
| `BN8M 66 9RX` | | | | 6CD | None | No | No | No | No | Fascia | 2004 Mazda 3, USDM (After 01.08.2004) |
| `BN8S 66 9RXA` | | | | 6CD | None | No | No | No | No | Fascia | 2004 Mazda 3, USDM (BK1** -215691) (Before 01.08.2004) |
| `BR9A 66 9RX` | | | | 6CD | None | No | No | Yes | No | Fascia | 2004 Mazda 3, USDM (BK1** 244433-) (After 01.11.2004) |
| `BN8R 66 9S0` | | | | None | None | No | No | No | No | Fascia | 2004 Mazda 3, USDM (BK1** 244433-) (After 01.08.2004) |
| `BN8P 66 9S0A` | | | | None | None | No | No | No | No | Fascia | 2004 Mazda 3, USDM (BK1** -215691) (Before 01.08.2004) |
| | FMS | | `BN8P 66 AS0` | | | | | No  | No | Fascia | 2004 Mazda 3, USDM (Before 01.08.2004) |
| | FMS | | `BN8R 66 AS0` | | | | | No  | No | Fascia | 2004 Mazda 3, USDM (After 01.08.2004) |
| | FMS | | `BR9A 66 AS0` | | | | | Yes | No | Fascia | 2004 Mazda 3, USDM (After 01.11.2004) |
| ASM | OE | OE PN | Maz PN | CD? | Tape? | Set? | Nav? | BOSE? | AUX? | LCD? | 2004 Mazda 3, xxx |
| `BP4M 66 950A` | Sanyo | |  | 1CD | No | **Info** | | (?) | | **Fascia** | 2006 Mazda 3, Latvia,UK |
| `BP4L 66 950A` | FMS (?) | | | 1CD | Yes | | | | | **Fascia** | 2004 Mazda 3, Lithuania |
| | Clarion | PT-2770F | `BR9K 66 ARX` | Yes | No | **Info** | | (?) | Yes | MSCAN | 2006-2007 Mazda 3, USDM (Silver) |
| | Clarion | PT-2770F | `BR9K 66 ARX` | Yes | No | **Info** | | (?) | Yes | MSCAN | 2006-2007 Mazda 3, USDM |
| `BAN8 66 9R0` | Sanyo | 14797178 | `BR9E 66 AR0` | Yes | No | Set | | (?) | Yes | MSCAN | 2006-2007 Mazda 3, USDM (Silver) |
| `BAP7 66 9R0` | Sanyo | 14798188 | `BAP5 66 AR0` | Yes | No | :00 | | (?) | Yes | MSCAN | 2007-2009 Mazda 3, USDM (Silver)|
| | Sanyo | 14794157 | `BCB1 66 ARX` | 6CD MP3 | No | Set | | No | Yes | MSCAN | 2008 Mazda 3, AUDM |
| | Sanyo | 14795257 | `BCB1 66 ARX` | 6CD MP3 | No | Set | | (?) | Yes | MSCAN | 2007-2009 Mazda 3, AUDM |
| | Sanyo | 14795847 | `BAK3 66 ARX` | 6CD MP3 | No | Set | | (?) | Yes | MSCAN | 2008 Mazda 3, AUDM (Gloss Black)|
| | Sanyo | 14797148 | `BR9F 66 AR0` | Yes | No | **Info** | | (?) | Yes | MSCAN | 2007 Mazda 3, USDM |
| `BAP2 66 9RX` | Clarion | PT-2769F | `BR9G 66 ARX` | 6CD | No | Set | | (?) | Yes | MSCAN | 2006-08 Mazda 3, USDM (Silver) |
| `BR9E 66 9R0A` | Sanyo | 14794078 | `BR9E 66 AR0` | Yes | No | Set | | (?) | Yes | MSCAN | 2006-2007 Mazda 3, USDM |
| `BAP5 66 9R0` | Sanyo | 14798188 | `BAP5 66 AR0` | 1CD | No | :00 | | (?) | Yes | MSCAN | 2008 Mazda 3, USDM |
| | Sanyo | 14793433 | `BAS5 79 EG0` | 1CD MP3 | No | :00 | | (?) | Yes | MSCAN | 2008 Mazda 3, USDM |
| `BR9K 66 9RXA` | Clarion | PT-277?F | `BR9K 66 ARX` | 6CD | No | **Info** | | (?) | | MSCAN | 2006 Mazda 3, USDM (JM1BK323761411071)
| `BAR3 66 9RX` | Clarion | PT-2770F | `BAR3 66 ARX` | Yes | No | **Info** | | (?) | Yes | MSCAN | 2006-2007 Mazda 3, USDM (Silver) |
| | Sanyo | 14794532 | `BS0A 79 EG0` | 6CD MP3 | No | Set | | (?) | Yes | MSCAN | 2007 Mazda 3, USDM |
| `BAP9 66 9RX` | Clarion | PT-2769F | `BAP9 66 ARX` | 6CD | No | :00 | | (?) | Yes | MSCAN | 2008 Mazda 3, USDM |
| | Sanyo | 14794748 | `BCB? 66 AR0` | 1CD MP3 | No | Set | | No | Yes | MSCAN | 2008 Mazda 3, AUDM |
| | Sanyo | 14794018 | `BR2D 66 AR0` | 1CD | No | Set | | (?) | | MSCAN | 2005 (!?) Mazda 3, UKDM (JMZBK14Z501320559) (Has Media button, maybe a swap) |
| `BR2D 66 9R0A` | Sanyo |  | `BR2D 66 AR0` | 1CD | No | Set | | (?) | | MSCAN | 2006 Mazda 3, UKDM (JMZBK14Z201338928) |
| `BS0A 66 9RX` | Clarion | PT-2769F | `BR9G 66 ARX` | 6CD | No | Set | | | (?) | | MSCAN | 2005 Mazda 3, USDM |
| `BAR2 66 9RX` | Clarion | PT-2774F | `BAR2 66 ARX` | Yes | No | **Info** | | | Yes | MSCAN | 2008 Mazda 3, USDM (Silver) |
| `BAR9 66 9RXA` | Sanyo | 14793688 | `BAR9 66 ARX` | 6CD MP3 | No | :00 | | | Yes | MSCAN | 2009 Mazda 3, USDM |
| | Sanyo | 14794008 | `BR2B 66 AR0` | Yes | No | **Info** | | | Yes | MSCAN | 2006 Mazda 3, DEU DM |
| `BS3N 66 9R0` | Sanyo | 14794018 | `BR2D 66 AR0` | 1CD | No | Set | | | Yes | MSCAN | 2006 Mazda 3, UKDM, Silver |
| `BS3J 66 9R0` | Sanyo | 14794008 | `BR2B 66 AR0` | 1CD | No | **Info** | | | Yes | MSCAN | 2006 Mazda 3, DEU DM |
| `BR1B 66 9RX` | Clarion | PT-2782F | `BR2V 66 ARX` | 6CD | No | **Info** | | | Yes | MSCAN | 2005 Mazda 3, UKDM |
| `BS2T 66 9R0` | Sanyo | 14798108 | `BS3T 66 AR0` | 1CD | No | **Info** | | | Yes | MSCAN | DEU DM (Silver) |
| `B32H 66 9S08` | Sanyo | 14789939 | `B32H 66 AS0` | No | No | Set | | | No | Fascia | 2004 JDM |
| `B32L 66 9R0B` | Sanyo | | | 1CD | No | Set | | | No | Fascia | 2003 JDM |
| `B33A 66 9R0`  | Sanyo | 14796038 | `B33A 66 AR0` | 1CD | No | | | Yes | MSCAN | 2006+ JDM |


Some notes:

* Pre "01.08.2004" models appear to have different brackets between 5HB and 4SD. This looks to be unified afterwards.
* The 2004 models all appear to only have three base core modules (`BP0S`,`BP4K`,`PVSF` -`66 AS0`) with varying tuner steps. This implies the variations are Tuner type (9/50KHz (AU/US), Taiwan, Columbia) x CD (no, 1CD, 6CD) x bracket type (4SD, 5HB, combined)

Of note is that the fascia does swap most of the buttons around between LHD and RHD, and yet there is no clear marking on the fascia or difference on the core module.

## Core module

What I'm calling the "core module" consists of the following functions:

* fascia button handling
* AM / FM receiver
* Audio input switching
* Audio adjustment (volume, treble, bass, balance, fader)
* Audio amplifying (applicable only to non-BOSE units)
* CAN-BUS interaction (used for ALC, also apparently for climate control?)
* Navigation system interfacing (unknown, makes beep sound on command perhaps?)

This module is a single board and (in these case at least) the CD module slots in the top (note the metal clips and screws) and the cassette module goes in underneath.

The Core module itself is 1DIN, the cassette module takes up the empty space in the bottom, and the CD module adds another 1DIN of space to the top.

### Main board

#### FMS 14789959 (BP0S 66 AS0)

Single layer construction.

| Part Label | Marking | Model number | Description | Role | Location |
| --- | --- | --- | --- | --- | --- |
| CN501 | |        | 24 pin harness     | Primary harness connector (Power, audio, UART, CAN, illum) | Rear right |
| CN601 | |        | 14 pin brd2brd     | Fascia interconnect | Front edge, RHS |
| CN602 | |        | 14 pin brd2brd     | Fascia interconnect | Front edge, LHS |
| CN801 | |        | 16 pin brd2brd     | Upper module (via carrier board) (CD / CD Stacker) | Bottom, left side |
| CN802 | |        | 15 pin brd2brd     | Lower module (MD / Cassette)  | Bottom; rear centre |
| CN803 | |        | 2x8 pin connector  | Auxiliary media input (ext. CD stacker, satellite radio)   | Rear left |
| IC001 | LC72135  | Sanyo LC72135    | PLL Freq Synth for Tuning           | Generates frequency for FM tuner module | Center Left |
| IC401 | TDA7437  | ST TDA7437       | Digital Cont. Audio Processor       | Switches, adjusts audio signals         | Center (slightly left) |
| IC501 |          |                  | Amplifier                           | Audio Power amp                         | Rear (center of heat block) |
| IC601 | MB90549G | Fujitsu MB90F549 | 16-bit Proprietary Microcontroller  | Controls the other chips                | Front Left (Towards center) |
| IC602 | C02      | 24C02            | 256x8 EEPROM                        | Stores anti-theft code in other models  | Front Left (Near LHS fascia connector) |
| IC605 | TJA1050  | Philips TJA1050  | High speed CAN transceiver          | CANBUS interface driver for MSCAN       | Rear Right (Near cushion) |
| IC607 | TC4053   | Toshiba TC4053   | Triple 2-channel multiplexer        | Switches CD/MD-DATA/CLK between modules | Front Right (Near RHS fascia connector) |

##### IC001 - LC72135 PLL oscillator

##### IC401 - TDA7437 Audio controller

##### IC501 - Power amp

##### IC601 - MB90F549 MCU

Since this is a large chip, I'm only going to trace out items of note.

| Pin number | Label | Type | Connected to |
| --- | --- | --- | --- |
| 18 | | | Main connector - U2 - to Satellite Navigation Unit |
| 20 | | | Main connector - U1 - to Satellite Navigation Unit |
| 27 | | | IC607 #15 - from MD/CD `TEXT-CL` |
| 29 | | | IC607 #14 - from MD/CD `TEXT-DA` |
| 65 | | | IC607 #10 & #11 - select between MD and CD TEXT |

##### IC602 - 24C02 EEPROM

Appears to be used to store the anti-theft code. Perhaps this also decides if the UART is used (for the navigation system), the left- or right- handedness, and if the INFO button is used?

##### IC605 - TJA1050 CAN Transciever

| Pin number | Label | Type | Connected to |
| --- | --- | --- | --- |
| 1 | TXD  | TTL CAN Input  | |
| 2 | GND  | Ground         | |
| 3 | VCC  | Power          | |
| 4 | RXD  | TTL CAN Output | |
| 5 | VREF | Reference      | |
| 6 | CANL | CANBUS         | |
| 7 | CANH | CANBUS         | |
| 8 | S    | Toggle         | |

##### IC607 - TC4053 Mux

"Triple 2-channel multiplexer" - Select input "0" or "1" for pair "X", "Y" or "Z" (i.e. three sets of a two input, one output switch).

| Pin number | Label | Type | Connected to |
| --- | --- | --- | --- |
|  1 | 1Y    | Input     | Upper Module #9 - `TEXT-CL` & D632 |
|  2 | 0Y    | Input     | Lower Module #9 - `TExT-CL` & D632 |
|  3 | 1Z    | Input     | NC (?) |
|  4 | Z out | Output    | NC (?) |
|  5 | 0Z    | Input     | NC (?) |
|  6 | INH   | Inhibit   | GND |
|  7 | VEE   | Power (Negative) | GND |
|  8 | VSS   | Ground    | GND |
|  9 | C     | Input (Z) | GND |
| 10 | B     | Input (Y) | Combined with #11, goes to IC601 #65|
| 11 | A     | Input (X) | Combined with #10, goes to IC601 #65|
| 12 | 0X    | Input     | Upper Module #10 - `TEXT-DA`, looks to be pulled high or low |
| 13 | 1X    | Input     | Lower Module #10 - `TEXT-DA` via D632  |
| 14 | X out | Output    | IC601 #29 |
| 15 | Y out | Output    | IC601 #27 | 
| 16 | VDD   | Power     |

MCU drives `A` and `B` to select between the MD and CD unit's text output. Unsure of the protocol yet. `C` is grounded, and there's no wiring to the auxiliary connector.

### Fascia

The bottom section of the fascia consists of a panel that clips over the two knobs and either blanks the hole, or provides a loading slot and button(s) for the cassette / MD module.

The top section similarly blanks out the CD loader slot if a CD module is not present. This part of the fascia has different labelling based on if it's a non-changer or 6CD, and if it's MP3 capable or not. The EJECT button is on the main section of the fascia, so it's blocked off, and then signalled through the core module.

### Fascia interface

There are two connectors (left and right) that connects the core module to the fascia. At this stage, this is of little practical interest.

### Module interface

The Core module provides three interfaces to connect other audio sources. Naturally, two of these are for the bottom and top modules, and a third is the "other" port on the back. This port is designed for an external CD Changer or satellite radio unit.

The following signals appear to be the typical "module interface":

| Pin | Name | Type | Direction | Protocol | External port | Notes |
| --- | --- | --- | --- | --- | --- | --- | 
|  1 | BUS+ | Data | Bi-Directional | M-Bus | Yes |
|  2 | BUS GND | Ground | To core | GND | Yes | Seems to just be connected to normal ground directly |
|  3 | B+ | Power | To module | Power | Yes | "Always on"(?) 12V battery supply |
|  4 | B+ | Power | To module | Power | Yes | See above |
|  5 | GND | Ground | To core | GND | Yes | |
|  6 | GND | Ground | To core | GND | Yes | |
|  7 (CD) | Eject | Signal | To module (?) | Unknown | Unknown | |
|  8 (CD) | NC | | | | | |
|  7 (Tape) | TNS | Power / Signal | To module | Active high / Power | Yes | Activates when lights are on |
|  8 (Tape) | ILLUM(-) | Ground | To core | GND | Yes | Light adjustment / power return|
|  9 | TEXT-CLOCK | Data | To module (?) | Unknown | Unknown | |
| 10 | TEXT-DATA | Data | To core (?) | Unknown | Unknown | |
| 11 | ACC | Signal | To module | Active high | Yes | Power-up signal from key switch |
| 12 | SYSMUTE | Signal | To core (?) | Unknown | Unknown | Signals the head unit to mute audio - perhaps when seeking / preparing? |
| 13 | RIGHT | Audio | To core | Line-level audio, right channel | Yes | |
| 14 | RIGHT | GND | To module | Audio Ground | Yes | |
| 15 | RIGHT | Audio | To core | Line-level audio, left channel | Yes | |

## CD module

Known models:

| Vendor | Vendor Model Number | Mazda Part Number | Capacity | MP3 capable |
| --- | --- | --- | --- | --- |
| Panasonic | `CX-CM3291AK` | `GJ6J 66 EG0` | 6CD | No |
| | | `F153 66 AG0` | 1CD | No |
| | | `B30K 66 AG0` | 1CD | No |
| | | `B30K 66 AG0A` | 1CD | No |
| | | `F153 66 AG0` | 1CD | No |
| | | `B30K 66 AG0` | 1CD | No |
| | | `B30K 66 AG0A` | 1CD | No |
| FMS | | `GK1R 66 AG0` | 1CD | No |

Fascia without CD is `BP4K 66 BG4`, 1CD `B32L 66 BG4`, 6CD `B32P 66 BG4`.

### 6CD variant

## Tape module

Known models:

| Vendor | Vendor Model Number | Mazda Part Number | Remarks |
| --- | --- | --- | --- |
| FMS | | `B30F 66 AD0` | Part type: "66-900D" - "FMS AUDIO; W/CASSETTE"

As the tape module "brings" it's own fascia, it passes the TNS & ILLUM signals to the new fascia. The connector's pinout is as follows:

| Pin | Name | Type | Direction | Protocol | Notes |
| --- | --- | --- | --- | --- | --- |
|  1 | GND | Ground | To module | GND | |
|  2 | DOLBY SW | Signal | To module | Signal (?) | Dolby Noise Reduction button (?) |
|  3 | EJECT SW | Signal | To module | Signal (?) | Eject button |
|  4 | ILL 8V | Power / Signal | To fascia | Illumination LED power | |
|  5 | ILL 8V | Power / Signal | To fascia | Illumination LED power | |
|  6 | GND | Ground | To core | GND | Yes | |
|  7 | GND | Ground | To core | GND | Yes | |
|  6 | NC | | | | | |
|  7 | NC | | | | | |
|  8 | NC | | | | | |
|  9 | NC | | | | | |
| 10 | NC | | | | | |
| 11 | NC | | | | | |
| 12 | NC | | | | | |

The fascia to suit the Mazda 3 units is `BP0T 66 BC0`, without tape it is `BP4K 66 BC0`

## MiniDisc module

Known models:

| Vendor | Vendor Model Number | Mazda Part Number | Remarks |
| --- | --- | --- | --- |

Similarly, the MD also needs it's own fascia. Pinout is not known to me.
